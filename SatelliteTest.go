package main

import (
	"fmt"
	"gxs3/demeter/common/net"
)

func main2() {
	client := net.NewClient("tcp://localhost:5555", false)
	defer client.Close()
	service := []byte("echo")
	message := []byte("hello world")

	count := 0
	for ; count < 10000; count++ {
		request := [][]byte{message}
		reply := client.Send(service, request)

		if len(reply) == 0 {
			break
		}
	}

	fmt.Printf("%d requests/replies processed\n", count)
}
