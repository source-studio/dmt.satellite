/*
 Main application package for the satellite node. This will start up the satellite
*/
package main

import (
	ca "gxs3/demeter/common/app"
	"gxs3/demeter/common/model"
	sa "gxs3/demeter/satellite/app"
	"gxs3/godfather/data"
	"os"
)

var (
	template   *data.SqlTemplate // SQL Template used to perform database transactions
	dictionary map[string]string // Query dictionary
)

/*
 Main satellite node application entry point
*/
func main() {
	var config = sa.NewConfig()
	var err = config.Read()

	if err != nil {
		os.Stderr.WriteString("Configuration error: " + err.Error() + "\n")
	}

	err = dbInit(config)

	if err != nil {
		os.Stderr.WriteString("Database error: " + err.Error() + "\n")
		return
	}

	var router = ca.NewRouter(config.BrokerAddress(), config.SatelliteService())
	sth, err := sa.NewSatelliteHandler(router, template, dictionary)

	if err != nil {
		os.Stderr.WriteString("Handler init error: " + err.Error() + "\n")
		return
	}

	sth.Init()
	sensorInit(config)
	router.Start()
}

func Template() *data.SqlTemplate {
	return template
}

func Dictionary() map[string]string {
	return dictionary
}

/*
 Initializes the database modules based on configuration parameters
*/
func dbInit(config *sa.Config) error {
	pool, err := data.NewConnectionPool(config.Driver(), config.DataSource(), config.PoolSize())

	if err != nil {
		return err
	}

	template, err = data.NewSqlTemplate(pool)

	if err != nil {
		return err
	}

	dictionary = make(map[string]string)

	for _, qf := range config.QueryFiles() {
		queries, err := data.ReadQueryFile(qf)

		if err != nil {
			return err
		}

		for key, query := range queries {
			dictionary[key] = query
		}
	}

	return nil
}

/*
 Initializes sensor modules based on configuration parameters
*/
func sensorInit(config *sa.Config) {
	for _, sensor := range config.Sensors() {
		if !sensor.On() {
			continue
		}

		//TODO Fill in sensor specific Agents when implemented

		switch sensor.Type() {
		case model.PH:
		case model.CONDUCTIVITY:
		case model.TEMPERATURE:
		case model.HUMIDITY:
		case model.RADIATION:
		case model.OXYGEN:
		case model.FLOW:
		case model.PRESSURE:
		}
	}
}
