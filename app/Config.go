package app

import (
	"errors"
	"fmt"
	"gxs3/demeter/common/app"
	"gxs3/demeter/common/model"
	"os"
	"strings"
)

const (
	SATELLITE_SERVICE = "net.satellite-service" // Service name that will identify the satellite on the network
	BROKER_ADDRESS    = "net.broker-address"    // TCP address to connect to broker. Should include port
	BROKER_SERVICE    = "net.broker-service"    // Service name that will identify broker on the network
	DRIVER            = "db.driver"             // Database driver
	DATA_SOURCE       = "db.data-source"        // Database data source. If sqlite is the driver, a filename is expected
	POOL_SIZE         = "db.pool-size"          // Number of concurrent connections to open to the database.
	QUERY_FILES       = "db.query-files"        // Array of files that contain queries. Will populate the query dictionary.
	SENSOR_LIST       = "sensor.list"           // List of sensors attached to this satellite
	SENSOR_TYPE       = "sensor.type"           // Type of sensor
	SENSOR_ON         = "sensor.on"             // Is the sensor on? Should the satellite take readings from it?
	SENSOR_TRIGGER    = "sensor.trigger"        // Defines the condition that will drive the satellite to take readings from the sesor
	SENSOR_CONNECTOR  = "sensor.connector"      // Defines details to make serial connection to the sensor
	TRIGGER_TYPE      = "trigger.type"          // Defines the type of event that will initiate sensor readings
	TRIGGER_VALUE     = "trigger.value"         // Argument for the trigger type
	CONNECTOR_PORT    = "connector.port"        // Port or device to connect to
	CONNECTOR_BAUD    = "connector.baud"        // Baud rate to connect with

	CONFIG_PATH = "/.config/demeter/satellite-config.json" // Default location for the satellite configuration file
)

/*
 Configuration model. Holds configuration values as read from file for easy access from the rest of application. Also
 allows to rewrite configureation file with the value it holds, for easy persistence of changes.
*/
type Config struct {
	*app.BaseConfig
	satelliteService string          // Service name that will identify the satellite on the network
	brokerAddress    string          // TCP address to connect to broker. Should include port
	brokerService    string          // Service name that will identify broker on the network
	driver           string          // Database driver
	dataSource       string          // Database data source. If sqlite is the driver, a filename is expected
	poolSize         uint            // Number of concurrent connections to open to the database.
	queryFiles       []string        // Array of files that contain queries. Will populate the query dictionary.
	sensors          []*model.Sensor // List of sensors attached to this satellite
}

/*
 Creates a new configuration holder
*/
func NewConfig() *Config {
	var c = new(Config)
	c.BaseConfig = new(app.BaseConfig)

	return c
}

/*
 Reads configuration from the default destination
*/
func (c *Config) Read() error {
	var path = os.Getenv("HOME") + CONFIG_PATH
	var job, err = c.BaseConfig.Read(path)

	if err != nil {
		return err
	}

	var home = os.Getenv("HOME")
	var source = strings.Replace(job[DATA_SOURCE].(string), "${HOME}", home, -1)
	var qfr []interface{}
	var satServ, brokerAdd, brokerServ, driver string
	var poolSize float64
	var sensorja []interface{}
	var ok = make([]bool, 7)

	qfr, ok[0] = job[QUERY_FILES].([]interface{})
	var queryFiles = make([]string, len(qfr))

	for cont, qf := range qfr {
		queryFiles[cont] = qf.(string)
		queryFiles[cont] = strings.Replace(queryFiles[cont], "${HOME}", home, -1)
	}

	satServ, ok[1] = job[SATELLITE_SERVICE].(string)
	brokerAdd, ok[2] = job[BROKER_ADDRESS].(string)
	brokerServ, ok[3] = job[BROKER_SERVICE].(string)
	driver, ok[4] = job[DRIVER].(string)
	poolSize, ok[5] = job[POOL_SIZE].(float64)
	sensorja, ok[6] = job[SENSOR_LIST].([]interface{})

	for _, o := range ok {
		if !o {
			return errors.New("Configuration: An element in the global section of the config has an incorrect value")
		}
	}

	errs := make([]error, 7)
	errs[0] = c.SetSatelliteService(satServ)
	errs[1] = c.SetBrokerAddress(brokerAdd)
	errs[2] = c.SetBrokerService(brokerServ)
	errs[3] = c.SetDriver(driver)
	errs[4] = c.SetDataSource(source)
	errs[5] = c.SetPoolSize(uint(poolSize))
	errs[6] = c.SetQueryFiles(queryFiles)

	for _, errn := range errs {
		if errn != nil {
			return errn
		}
	}

	c.sensors = make([]*model.Sensor, len(sensorja))
	ok = make([]bool, 4)
	var tjob, cjob map[string]interface{}
	var styperaw, ttyperaw, port string
	var on bool
	var rawval, baud float64
	var value float32

	for cont, sjr := range sensorja {
		sjob, sko := sjr.(map[string]interface{})

		if !sko {
			return errors.New("Configuration: At least one sensor is not defined as an object")
		}

		styperaw, ok[0] = sjob[SENSOR_TYPE].(string)
		on, ok[1] = sjob[SENSOR_ON].(bool)
		tjob, ok[2] = sjob[SENSOR_TRIGGER].(map[string]interface{})
		cjob, ok[3] = sjob[SENSOR_CONNECTOR].(map[string]interface{})

		for _, o := range ok {
			if !o {
				return errors.New("Configuration: At least one sensor element was not defined as expected")
			}
		}

		ttyperaw, ok[0] = tjob[TRIGGER_TYPE].(string)

		if !ok[0] {
			return errors.New("Configuration: At least one trigger.type value was not defined as a string")
		}

		rawval, ok[1] = tjob[TRIGGER_VALUE].(float64)
		value = 0

		if !ok[1] {
			value = float32(rawval)
		}

		var trigger, err = model.NewTrigger(
			model.TriggerType(ttyperaw),
			value,
		)

		if err != nil {
			return err
		}

		port, ok[0] = cjob[CONNECTOR_PORT].(string)
		baud, ok[1] = cjob[CONNECTOR_BAUD].(float64)

		for cont := 0; cont < 2; cont++ {
			if !ok[cont] {
				return errors.New("Configuration: At least one sensor.connector object was not defined properly")
			}
		}

		connector, err := model.NewConnector(
			port,
			int(baud),
		)

		c.sensors[cont], err = model.NewSensor(
			uint(cont),
			c.satelliteService,
			model.SensorType(styperaw),
			on,
			trigger,
			connector,
		)

		if err != nil {
			return err
		}
	}

	return nil
}

/*
 Writes configuration to the default destination
*/
func (c *Config) Write() error {
	var home = os.Getenv("HOME")
	var path = home + CONFIG_PATH
	var sensors = make([]interface{}, len(c.sensors))

	for cont, sensor := range c.sensors {
		var tjob = map[string]interface{}{
			TRIGGER_TYPE: sensor.Trigger().Type(),
		}

		if sensor.Trigger().Type() != model.EVENT {
			tjob[TRIGGER_VALUE] = sensor.Trigger().Value()
		}

		var cjob = map[string]interface{}{
			CONNECTOR_PORT: sensor.Connector().Port(),
			CONNECTOR_BAUD: sensor.Connector().Baud(),
		}

		sensors[cont] = map[string]interface{}{
			SENSOR_TYPE:      sensor.Type(),
			SENSOR_ON:        sensor.On(),
			SENSOR_TRIGGER:   tjob,
			SENSOR_CONNECTOR: cjob,
		}
	}

	var job = map[string]interface{}{
		SATELLITE_SERVICE: c.satelliteService,
		BROKER_ADDRESS:    c.brokerAddress,
		BROKER_SERVICE:    c.brokerService,
		DRIVER:            c.driver,
		DATA_SOURCE:       strings.Replace(c.dataSource, home, "${HOME}", -1),
		POOL_SIZE:         c.poolSize,
		QUERY_FILES:       c.queryFiles,
		SENSOR_LIST:       sensors,
	}

	return c.BaseConfig.Write(path, job)
}

func (c *Config) SatelliteService() string {
	return c.satelliteService
}

func (c *Config) SetSatelliteService(satelliteService string) error {
	if len(satelliteService) < 1 {
		return errors.New("Configuration: Satellite service is empty")
	}

	c.satelliteService = satelliteService
	return nil
}

func (c *Config) BrokerAddress() string {
	return c.brokerAddress
}

func (c *Config) SetBrokerAddress(brokerAddress string) error {
	if len(brokerAddress) < 1 {
		return errors.New("Configuration: Broker Address is empty")
	}

	c.brokerAddress = brokerAddress
	return nil
}

func (c *Config) BrokerService() string {
	return c.brokerService
}

func (c *Config) SetBrokerService(brokerService string) error {
	if len(brokerService) < 1 {
		return errors.New("Configuration: Broker Service is empty")
	}

	c.brokerService = brokerService
	return nil
}

func (c *Config) Driver() string {
	return c.driver
}

func (c *Config) SetDriver(driver string) error {
	if len(driver) < 1 {
		return errors.New("Configuration: Driver is empty")
	}

	c.driver = driver
	return nil
}

func (c *Config) DataSource() string {
	return c.dataSource
}

func (c *Config) SetDataSource(dataSource string) error {
	if len(dataSource) < 1 {
		return errors.New("Configuratoin: Data Source is empty")
	}

	c.dataSource = dataSource
	return nil
}

func (c *Config) PoolSize() uint {
	return c.poolSize
}

func (c *Config) SetPoolSize(poolSize uint) error {
	if poolSize < 1 {
		return errors.New("Configuration: Pool Size has to be a number greater than 0")
	}

	c.poolSize = poolSize
	return nil
}

func (c *Config) QueryFiles() []string {
	return c.queryFiles
}

func (c *Config) SetQueryFiles(queryFiles []string) error {
	if len(queryFiles) < 1 {
		return errors.New("Configuration: Query files array is empty")
	}

	for cont, qf := range queryFiles {
		if len(qf) < 1 {
			return errors.New(fmt.Sprintf("Configuration: Entry #%d in query files is empty", (cont + 1)))
		}
	}

	c.queryFiles = queryFiles
	return nil
}

func (c *Config) Sensors() []*model.Sensor {
	return c.sensors
}

func (c *Config) SetSensors(sensors []*model.Sensor) {
	if sensors == nil {
		sensors = make([]*model.Sensor, 0)
	}

	c.sensors = sensors
}
