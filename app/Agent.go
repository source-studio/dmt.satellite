package app

import (
	"errors"
	"fmt"
	dm "gxs3/demeter/common/model"
	gm "gxs3/godfather/model"
	"time"
)

var (
	/*
		Shared event bus among all agents. This is used for inter-agent communication, and for communication of other
		modules to agents. Communication will be made in the form of events. When agens registered to an event will
		consume it, and process it according to its own logic.
		It takes the form of a map. Keys are Event Types. Values are slices of event channels. Each event channel
		comes from an agent. All agents subscribed to an event type will be notified via the bus of the event
		so they can process it.
	*/
	bus = make(map[dm.EventType][]chan dm.Event)
)

/*
 An agent combines several predefined concepts and makes them work together to achieve the common goal of sending readings
 to the broker. It takes a sensor; agent implementations will wrap this sensor in an adapter to take readings from it.
 It has an operator, which is a high lievel logical client. This will be used to send readings to the broker. It has a
 channel. This channel will be used to communicate with other agents or external modules via events. These events will
 alert the agent when a reading needs to be taken from a sensor, or any other type of operation needs to be performed.
*/
type Agent struct {
	sensor   *dm.Sensor         // Sensor from which readings will be taken
	operator *SatelliteOperator // Operator that will send readings to broker
	channel  chan dm.Event      // Channel for inter-agent and external communication
}

/*
Creates a new agent. Requires the sensor it will work with, as well as the operator it will use to send readings to the
broker.
*/
func NewAgent(sensor *dm.Sensor, operator *SatelliteOperator) (*Agent, error) {
	if sensor == nil || operator == nil {
		return nil, gm.NIL_ARGUMENT
	}

	return &Agent{sensor, operator, make(chan dm.Event)}, nil
}

/*
 Initializes the agent. Specific initialization logic will depend on the sensors trigger type. If it's a time trigger,
 a ticker will be set up. If it's an event or alarm trigger, listeners will be initiated to response to such events.
*/
func (a *Agent) Init() error {
	switch a.sensor.Trigger().Type() {
	case dm.TIME:
		a.initTime()
	case dm.EVENT:

	case dm.ALARM:

	default:
		message := fmt.Sprintf("Unexpected trigger type '%s' on sensor %s", a.sensor.Trigger().Type(), a.sensor.Type())
		return errors.New(message)
	}

	return nil
}

/*
 Registers this agent to respond to the given event type
*/
func (a *Agent) Register(etype dm.EventType) error {
	if len(etype) < 1 {
		return gm.EMPTY_STRING
	}

	var existing, ok = bus[etype]

	if ok {
		existing = append(existing, a.channel)
	} else {
		bus[etype] = []chan dm.Event{a.channel}
	}

	return nil
}

/*
 Unregisters this agent from this event type
*/
func (a *Agent) Unregister(etype dm.EventType) error {
	if len(etype) < 1 {
		return gm.EMPTY_STRING
	}

	var existing, ok = bus[etype]

	if !ok {
		return nil
	}

	for cont, c := range existing {
		if c == a.channel {
			existing = append(existing[:cont], existing[cont+1:]...)
			break
		}
	}

	return nil
}

/*
 Fires the given event to the event bus. All agents subscribed to the event's type will be notified
*/
func (a *Agent) Fire(event dm.Event) error {
	if event == nil {
		return gm.NIL_ARGUMENT
	}

	var existing, ok = bus[event.Type()]

	if !ok {
		return nil
	}

	for _, c := range existing {
		if event.Stopped() {
			break
		}

		c <- event
	}

	return nil
}

func (a *Agent) Sensor() *dm.Sensor {
	return a.sensor
}

func (a *Agent) Operator() *SatelliteOperator {
	return a.operator
}

func (a *Agent) Channel() chan dm.Event {
	return a.channel
}

func (a *Agent) initTime() {
	var duration = time.Duration(int64(a.sensor.Trigger().Value()) * int64(time.Second))
	var ticker = time.NewTicker(duration)

	go func() {
		for {
			stamp := <-ticker.C
			var event, err = dm.NewTimeEvent(stamp)

			if err != nil {
				continue
			}

			a.channel <- event
		}
	}()
}
