package app

import (
	"gxs3/demeter/common/app"
	dd "gxs3/demeter/common/data"
	"gxs3/demeter/common/model"
	gd "gxs3/godfather/data"
)

/*
 Handler implementation for dealing with satellite data
*/
type SatelliteHandler struct {
	*app.Handler
	dao *dd.SatelliteDao // Database access object for satellite transactions
}

/*
 Creates a new satellite handler. Requires the router, to send and receives messages on the network and the sql template
 and query dictionary for database transactions.
*/
func NewSatelliteHandler(router *app.Router, template *gd.SqlTemplate, dict map[string]string) (*SatelliteHandler, error) {
	var handler, err = app.NewHandler(router)

	if err != nil {
		return nil, err
	}

	dao, err := dd.NewSatelliteDao(template, dict)

	if err != nil {
		return nil, err
	}

	var sh = &SatelliteHandler{handler, dao}
	return sh, nil
}

/*
 Initializes the handler. Registers listeners to all the message types this handler will process
*/
func (sh *SatelliteHandler) Init() {
	sh.Register(model.REQUEST, app.SATELLITE_GET, sh.get)
	sh.Register(model.REQUEST, app.SATELLITE_UPDATE, sh.update)
	sh.Register(model.REQUEST, app.SATELLITE_CALIBRATE, sh.calibrate)
}

/*
 Retrieves the satellite data.
*/
func (sh *SatelliteHandler) get(message []byte) error {
	var request, err = model.DecodeRequest(message, nil)

	if err != nil {
		return err
	}

	satellite, err := sh.dao.GetAll()

	if err != nil && err == gd.NOT_FOUND {
		var response = request.MakeResponse(app.NOT_FOUND, nil)
		return sh.Send(response, nil)
	} else if err != nil {
		return err
	}

	var response = request.MakeResponse(app.OK, satellite[0])
	return sh.Send(response, model.EncodeSatellite)
}

/*
 Updates the current satellite data
*/
func (sh *SatelliteHandler) update(message []byte) error {
	var request, err = model.DecodeRequest(message, model.DecodeSatellite)

	if err != nil {
		var response = request.MakeResponse(app.BAD_REQUEST, nil)
		return sh.Send(response, nil)
	}

	var satellite, ok = request.Body().(*model.Satellite)

	if !ok {
		var response = request.MakeResponse(app.BAD_REQUEST, nil)
		return sh.Send(response, nil)
	}

	records, err := sh.dao.Update(satellite)

	if err != nil {
		return err
	}

	var status uint = app.OK

	if records < 1 {
		status = app.NOT_FOUND
	}

	var response = request.MakeResponse(status, nil)
	return sh.Send(response, nil)
}

/*
 Initiates calibration procedures for a sensor on the satellite
*/
func (sh *SatelliteHandler) calibrate(message []byte) error {
	var request, err = model.DecodeRequest(message, nil)

	if err != nil {
		return err
	}

	var response = request.MakeResponse(app.NOT_IMPLEMENTED, nil)
	return sh.Send(response, nil)
}
