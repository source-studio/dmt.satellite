package app

import (
	"gxs3/demeter/common/app"
	"gxs3/demeter/common/model"
)

/*
 Operator implementation for the satellite. Will handle satellite specific requests that need to be sent out to other
 nodes
*/
type SatelliteOperator struct {
	*app.Operator
}

/*
 Creates a new satellite operator. Needs to router for passing of messages and the service names by which this satellite
 and the broker are known on the netowrk.
*/
func NewSatelliteOperator(router *app.Router, satelliteService, brokerService string) (*SatelliteOperator, error) {
	var operator, err = app.NewOperator(router, satelliteService, brokerService)

	if err != nil {
		return nil, err
	}

	var so = &SatelliteOperator{operator}
	return so, nil
}

/*
 Sends a request to the broker to add a reading to the collection
*/
func (so *SatelliteOperator) addReading(reading *model.Reading) error {
	var request, err = model.NewRequest(app.READING_ADD, reading)

	if err != nil {
		return err
	}

	return so.Send(request, model.EncodeReading)
}
