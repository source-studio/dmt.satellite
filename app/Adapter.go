/*
 Satellite application logic package. This implements the logic that will initialize, configure and interact with sensors,
 take readings, and communicate these readings to the broker.
*/
package app

import (
	"github.com/tarm/goserial"
	dm "gxs3/demeter/common/model"
	gm "gxs3/godfather/model"
	"io"
)

/*
 An adapter provides a bridge between the application software and the physical sensor. An adapter will connect to a
 sensor, usually through a serial port, and retrieve readings based on trigger conditions.
*/
type Adapter struct {
	sensor *dm.Sensor         // Sensor this adapter will work with
	rwc    io.ReadWriteCloser // Connection used to communicate with sensor
}

/*
 Creates a new adapter, must receive the sensor that will interact with
*/
func NewAdapter(sensor *dm.Sensor) (*Adapter, error) {
	if sensor == nil {
		return nil, gm.NIL_ARGUMENT
	}

	var a = &Adapter{sensor, nil}
	return a, nil
}

/*
 Opens the connection to the sensor. Uses the connector details on the sensor model to make the connection.
*/
func (a *Adapter) Open() error {
	var config = &serial.Config{
		Name: a.sensor.Connector().Port(),
		Baud: a.sensor.Connector().Baud(),
	}

	var err error
	a.rwc, err = serial.OpenPort(config)
	return err
}

/*
 Close the connection to the sensor
*/
func (a *Adapter) Close() error {
	return a.rwc.Close()
}

/*
 Generic read from the sensor connection. The invoker should provide a buffer size to use to read from the connection.
 If the message goes over the buffer size, it will be truncated. This method only returns the message contents, not
 the padded buffer. Adapter implementations will provide higher level read implementations, it should not be required
 to invoke this directly for application logic.
*/
func (a *Adapter) Read(bufferSize uint) ([]byte, error) {
	if bufferSize < 1 {
		bufferSize = 128
	}

	var buffer = make([]byte, bufferSize)
	var rb, err = a.rwc.Read(buffer)

	if err != nil {
		return nil, err
	}

	return buffer[:rb], nil
}

/*
 Generic write to the sensor connection. The invoker will pass the message to write. This method returns the number
 of bytes written. Adapter implementations will provide higher level write implementations, it should not be required
 to invoke this directly for application logic.
*/
func (a *Adapter) Write(message []byte) (int, error) {
	return a.rwc.Write(message)
}

/*
 Returns a refernece to the sensor used by this adapter
*/
func (a *Adapter) Sensor() *dm.Sensor {
	return a.sensor
}
